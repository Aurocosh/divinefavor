#Divine favor
Magical mod centered around spiritual magic and spell casting.

#Author
Aurocosh (Ivchenko Sergey)

#Dependencies
To run this mod you will also need to install mods Patchouli and Forgelin

#Config
You can adjust most of items and mechanics through config files. This mod also supports CraftTweeker. 

#Credits
McJty - Thanks for the modding tutorial, it really helped to get me started in modding. His other projects also helped to learn modding.
Vazkii - Developer of mod Patchouli used by me for a guidebook. Also his open source projects helped to learn modding. And my messaging system was inpired by his AutoRegLib.
Angry-Pixel - Huge thanks for caving rope from their mod Betweenlands. I really liked it so i created several items based on this rope in my mod.
SlimeKnights - Thanks for Tinkers source i was able to figure out how to create delayed tasks like not instant tree chopping.
EPIIC_THUNDERCAT - His project Tameable Mobs helped me figure out how to work with mob entities and their AI
Lothrazar - His project Cyclic helped me figure out how to create my own potions. Even though there is no potions in my mod most of my over time effects based on mincraft potion system.
Direwolf20 - One of my debug items uses block selection based on block selection from his mod BuildingGadgets
Darkhax - Thanks for excellent tutorial on forge capabilities.